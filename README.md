超小型SCGUI全称simple Clour GUI,纯C语言开发可以移植到任意MCU上包括C51


优点：整个GUI约700行代码,采用局部刷新技术，基于双DMA缓存以线为最小单位绘制

240*320的SPI屏双DMA 内存开销最多u16 DMA_buf[320] *2  

作为一个现代GUI，支持背景图混色，支持半透明度，支持LVGL抗锯齿字体，和常规取模字体

支持文本动态移动，坐标超出Xend边界自动回到Xstart

支持基本图形支持抗锯齿画线，画圆，画圆矩形. 

控件包括基础文本，按键，波形, 可以设背景透明度 

注意：C51环境不支持C99，不能使用LVGL字库，只能使用常规取模字体


工程为Codeblocks 基于SDL2 仿真，包内有SDL2压缩包，将lib复制到
C:\Program Files\CodeBlocks\MinGW\x86_64-w64-mingw32\lib

仿真工程默认指定一个画点函数就可以运行，如果MCU支持DMA请打开DMA_WAP的宏，修改DMA接口

