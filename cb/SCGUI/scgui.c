
#include "scgui.h"


#if CPU_DMA_WAP
#include "lcd_9341.h"
//等待DMA完成中断标志，注意是放在DMA传送之前双BUF才有意义,等待是防止DMA未完成改变DS电平 LcdSelectWindows()
#define Wait_dma_end()  while(JL_SPI1->CON& BIT(13))
#endif

u16 DMA_buf_color[CPU_DMABUF_NUM][LCD_SCREEN_HEIGHT] = {0};  //两行内存1K
u16 *dma_prt=DMA_buf_color;         //切换双buf
u16  dma_buf_index = 0;
SC_GUI *gui;


//底层接口，与lvgl接口一样
void LCD_DMA_color(int xs, int ys, int xe, int ye, u16 *color)
{
    u16 width=xe-xs+1;
    u16 higth=ye-ys+1;
    u16 i,len=width*higth;
    u16 fc=gui->fc;
    u16 bc=gui->bc;
    if((gui->state&SC_STATE_IMAGE)==0)                           //刷图片不要进
    {
        if(gui->gbkg)
        {
            SC_Read_Image(xs, ys, width, higth,gui->gbkg,color);  //计算图片混合
        }
        else
        {
            if(gui->text)               //控件局部背景
            {
                fc=gui->text->fc;
                bc=SC_AlphaBlender(gui->text->bc,bc,gui->alpha);
            }
            else if(gui->alpha<255)    //
            {
                fc=SC_AlphaBlender(fc,bc,gui->alpha);
            }
            for(i=0; i<len; i++)
            {
                color[i]= SC_AlphaBlender(fc,bc,color[i]);
            }
        }
    }
#if CPU_DMA_WAP
    for(i=0; i<len; i++)
    {
        if(color[i])
        {
            color[i]=color[i]>>8|color[i]<<8; //高低位WAP
        }
    }
    //----------------DMA接口------------------
    Wait_dma_end();                   //放在LcdSelectWindows之前
    LcdSelectWindows(xs, ys,xe, ye);
    _DMA_write_buf((u8*)color,len*2);
#else
    //-----------------无DMA------------------------
    for(int y = ys; y < ys+higth; y++)
    {
        for(int x = xs; x < xs+width; x++)
        {
            gui->lcd_pset (x,y,*color);
            color++;
        }
    }
#endif
}
//DMA连续画点
void put_pset(u16 value)
{
    dma_prt[dma_buf_index++]=value;
}
//刷新屏幕
void LcdRefresh(int xs,  int ys,int xe, int ye)
{
    if(dma_buf_index==0)  return;
    xs=(xs+LCD_SCREEN_WIDTH)%LCD_SCREEN_WIDTH;
    xe=(xe+LCD_SCREEN_WIDTH)%LCD_SCREEN_WIDTH;
    ys=(ys+LCD_SCREEN_HEIGHT)%LCD_SCREEN_HEIGHT;
    ye=(ye+LCD_SCREEN_HEIGHT)%LCD_SCREEN_HEIGHT;
    //--------------DMA连续画点-------------------------
    if(ys>ye)
    {
        LCD_DMA_color(xs, 0,xe, ye,&dma_prt[LCD_SCREEN_HEIGHT-ys]);
        ye=LCD_SCREEN_HEIGHT-1;
    }
    if(xs>xe)
    {
        LCD_DMA_color(0, ys,xe, ye,&dma_prt[LCD_SCREEN_WIDTH-xs]);
        xe=LCD_SCREEN_WIDTH-1;
    }
    LCD_DMA_color(xs, ys,xe, ye,dma_prt);
    //-------------- 双BUF切换------------------------------
#if  (CPU_DMABUF_NUM==2)
    if(dma_prt==DMA_buf_color)
    {
        dma_prt= &DMA_buf_color[1][0];
    }
    else
    {
        dma_prt= &DMA_buf_color[0][0];
    }
#endif
    dma_buf_index = 0;
}

//显示背景图片
void SC_Show_Image(int xs,int ys,SC_img_t *bmp)
{
    int x,y;
    u16 *bkg=bmp->map;
    gui->state|= SC_STATE_IMAGE;          //刷图模式
    for(y=0; y<bmp->h; y++)
    {
        for(x=0; x<bmp->w; x++)
        {
            put_pset(bkg[x]);             //先复制到内存坐标检查用
        }
        LcdRefresh(xs, y+ys,xs+bmp->w-1,y+ys);
        bkg+=bmp->w;
    }
    gui->state&=~SC_STATE_IMAGE;          //刷图模式
    gui->gbkg=bmp;                         //用作背景
}

//读点函数,取背景图
void  SC_Read_Image(int xs,int ys,u16 w,u16 h,SC_img_t *img,u16 *buf)
{
    u16 y,x,bc;
    u16 *bkg=img->map;
    bkg+=img->w*ys+xs;
    if(gui->text)              //文字底色二次Alpha运算
    {
        for(y=0; y<h; y++)
        {
            for(x=0; x<w; x++)
            {
                bc= SC_AlphaBlender(gui->text->bc,bkg[x],gui->alpha);
                *buf= SC_AlphaBlender(gui->text->fc,bc,buf[0]);
                buf++;
            }
            bkg+=img->w;
        }
    }
    else
    {
        for(y=0; y<h; y++)
        {
            for(x=0; x<w; x++)
            {
                if(gui->alpha<255)
                {
                    buf[0]=buf[0]*gui->alpha>>8;
                }
                *buf= SC_AlphaBlender(gui->fc,bkg[x],buf[0]);
                buf++;
            }
            bkg+=img->w;
        }
    }
}

//初始化系统注册画点函数
void SC_GUI_Init(SC_GUI* g, void (*lcd_pset)(int,int,u16), u16 fc, u16 bc,void* font)
{
    static SC_OBJ mask;
    //-----------坐标限制区---------------
    mask.xs=0;
    mask.ys=0;
    mask.xe=240-1;
    mask.ye=320-1;

    g->lcd_pset= lcd_pset;

    g->alpha=255;               //全局透明度
    g->state=0;
    g->fc =fc;                  //GUI前景色，顶层
    g->bc =bc;                  //GUI背景色 ，底层
    g->font=font;                //GUI字库
    g->mask=&mask;              //GUI坐标限制
    g->gbkg=NULL;               //GUI背景图片
    gui = g;
    SC_FillFrame(0, 0, LCD_SCREEN_WIDTH-1, LCD_SCREEN_HEIGHT-1, 0);
}

//设置画点限制区，用于局部刷新
void SC_Set_Psetmask(int xs,int ys,int xe,int ye)
{
    gui->mask->xs=xs;
    gui->mask->xe=xe;
    gui->mask->ys=ys;
    gui->mask->ye=ye;
}

/******************************Alpha*******************************/
static u32 bc_Alpha,fc_Alpha,c_sum;     //临时变量快速运算alpha算法
//快速运算alpha算法 0-256
u32 SC_AlphaBlender(u16 fc,u16 bc,int Alpha)
{
    u32  result;
    if(Alpha<16)
    {
        return bc;
    }
    else if(Alpha>240)
    {
        return fc;
    }
    result=Alpha>>3;            //RGB565只有5位色
    if(c_sum!=(fc-bc))          //减少重复运算
    {
        c_sum=fc-bc;
        fc_Alpha = ( fc | ( fc<<16 ) ) & 0x7E0F81F;
        bc_Alpha = ( bc | ( bc<<16 ) ) & 0x7E0F81F;
    }
    result=((fc_Alpha - bc_Alpha) * result)>>5;
    result  = (result+bc_Alpha)& 0x7E0F81F;
    return (u32)(result&0xFFFF) | (result>>16);
}

//画横线
void SC_DrawHLine(int xs, int ys,int xe,u16 a)
{
    int w;
    for(w=xs; w<=xe; w++)
    {
        put_pset(a);
    }
    LcdRefresh(xs, ys,xe, ys);
}
//画竖线
void SC_DrawVLine(int xs, int ys,int ye,u16 a)
{
    int h;
    for(h=ys; h<=ye; h++)
    {
        put_pset(a);
    }
    LcdRefresh(xs, ys,xs, ye);
}
//实心矩形填充区
void SC_FillFrame( int xs, int ys, int xe, int ye, u16 a)
{
    int w,h;
    for(h=ys; h<=ye; h++)
    {
        for(w=xs; w<=xe; w++)
        {
            put_pset(a);
        }
        LcdRefresh(xs, h,xe, h);
    }
}
//空心矩形
void SC_DrawFrame( int xs, int ys, int xe, int ye, u16 a)
{
    SC_DrawHLine(xs,ys,xe,a);
    SC_DrawHLine(xs,ye,xe,a);
    SC_DrawVLine(xs,ys,ye,a);
    SC_DrawVLine(xe,ys,ye,a);
}

/**********************
* 写入编码数据，取模的单色位图，
* 取模设置，阴码，顺向C51取模从左到右
* width：字模宽
* higth: 字模高
* bpp: 兼容LVGL
*****************************/
void SC_Show_png(int xs,int ys,u16 width,u16 higth, u8 *buf,u8 bpp)
{
    int x,y;
    u8 a,i=0;
    u8 d=0xff<<(8-bpp);                 //0x80
    u8 size=(width*bpp+7)/8;            //取位图宽度
    u8 *map;
    for(x=xs; x<xs+width; x++)   //纵向解析效率更高
    {
        map=buf;
        if(gui->text)
        {
            if(x>gui->text->xe) return;
        }
        for(y=ys; y<ys+higth; y++)
        {
            a=*map<<i;              //alpha灰阶
            if(bpp==1)
            {
                a=(a&d)?a|(~d):a>>1;
            }
            put_pset(a);
            map+=size;             //纵向下一列
        }
        i+=bpp;
        if(i>=8)
        {
            i=0;
            buf++;
        }
        LcdRefresh(x, ys,x, y-1);           //纵向输出
    }
}
//----------------八分画圆----------------------
static u16 arc_w=0,arc_h=0,arc_fill=0;    //用于画圆角矩形
static void sc_pset3( int xs, int ys, int x, int s, int *a)
{
    if(gui->mask)                                       //局部刷新
    {
        if(xs<gui->mask->xs||xs>gui->mask->xe)  return;  //区外
        if(ys<gui->mask->ys||ys>gui->mask->ye)  return;  //区外
    }
    put_pset(a[1-x]);    //y-1
    put_pset(a[1]);      //y
    put_pset(a[1+x]);    //y+1
    if(s&0x01)                               //扇区判断
    {
        LcdRefresh(xs, ys-1,xs, ys+1);      //竖线
    }
    else
    {
        LcdRefresh(xs-1, ys,xs+1, ys);      //横线
    }
}

static void _circle_pixe(int xs,int ys,int x,int y,int *a)
{
    for(u8 i=0; i<3; i++)
    {
        if(a[i]<0)  a[i]=0;
    }
    sc_pset3(xs+y+arc_w, ys-x,-1,0,a);   //0-45
    sc_pset3(xs+x+arc_w, ys-y, 1,1,a);   //45-90
    sc_pset3(xs-x, ys-y,1,3, a);        //90-135
    sc_pset3(xs-y, ys-x,1,2, a);        //135-180
    sc_pset3(xs-y, ys+x+arc_h, 1, 4, a);  //180
    sc_pset3(xs-x, ys+y+arc_h,-1, 5, a);
    sc_pset3(xs+x+arc_w, ys+y+arc_h,-1,7, a);
    sc_pset3(xs+y+arc_w, ys+x+arc_h,-1,6, a);
}
static void _circle_line(int xs,int ys,int x,int y,int a)
{
    SC_DrawVLine(xs - y,      ys - x,      ys + x+arc_h, a);
    SC_DrawVLine(xs + y+arc_w,ys - x,      ys + x+arc_h, a);
    SC_DrawHLine(xs - x,      ys - y,      xs + x+arc_w, a);
    SC_DrawHLine(xs - x,      ys + y+arc_h,xs + x+arc_w, a);
}

//画圆反锯齿无浮点
#define sc_abs(x) ((int)x<0?x*-1:x)
void SC_FXAA_DrawCircle(int xs,int ys,int r,u8 s)
{
    int abuf[3]= {60,255,60};
    int x=0,y=r;
    int dS=0;
    int dE=3;
    int dSE=-(r<<1)+5;
    int d=1-r;
    int a=(2.8282*r);             //spqr(2)*2*r;
    while(1)
    {
        _circle_pixe(xs,ys,x,y,abuf);
        if(x>=y) break;
        if(d<0)
        {
            d+=dE;
            dE+=2;
            dSE+=2;
        }
        else
        {
            dS+=1-(y<<1);
            d+=dSE;
            dE+=2;
            dSE+=4;
            y--;
        }
        dS+=1+(x<<1);
        x++;
        abuf[0]=255-((dS+(y<<1)+1)*256)/a;
        abuf[1]=255-(sc_abs(dS)*256)/a;
        abuf[2]=255+((dS-(y<<1)+1)*256)/a;
        if(s)
        {
            if(y!=r)
            {
                _circle_line(xs,ys,x,y,255);
                if(x>y-1)
                {
                    abuf[2]=0xff;
                }
            }
        }
    }
    //----------------边线-----------------------------
    if(arc_w*arc_h)
    {
        _circle_line(xs,ys,0,r+1,60);
        _circle_line(xs,ys,1,r,230);
        if(s==0)
        {
            _circle_line(xs,ys,0,r-1,60);
        }
    }
    //----------------填充------------------------
    if(s)
    {
        SC_FillFrame( xs-x+1,  ys-x+1,  xs+x+arc_w-1,  ys+x+arc_h-1, 255);
    }
}

//圆角矩形
void SC_FXAA_RoundFrame(int xs,int ys,u16 w,u16 h,u16 r,u8 mode)
{
// SC_DrawFrame(xs, ys,xs+w-1,ys+h-1,120);      //边界清0
    if(r*2>w)  return;
    if(r*2>h)  return;

    arc_w= w-r*2-1;
    arc_h= h-r*2-1;
    SC_FXAA_DrawCircle(xs+r,ys+r,r,mode);
    arc_w=0;
    arc_h=0;
}

//文本控件坐标r倒角
void  SC_OBJ_text_cfg(SC_OBJ* text,int xs,int ys,u16 w,u16 h,u16 r)
{
    text->xs=xs;
    text->xe=xs+w-1;
    text->ys=ys;
    text->ye=ys+h-1;
    text->r=r;
}
//文本控件显示,不支持移动
void  SC_OBJ_text_show(SC_OBJ* text,int offsx,int offsy,char *str,u16 fc,u16 bc,u16 a)
{
    text->fc=fc;
    text->bc=bc;
    fc=gui->fc;
    gui->fc=bc;
    gui->alpha=a;
    //--------背景-----------
    if(text->r>1)
    {
        u16 w=text->xe-text->xs+1;
        u16 h=text->ye-text->ys+1;
        SC_FXAA_RoundFrame(text->xs,text->ys, w, h,text->r,1);
    }
    else
    {
        SC_FillFrame(text->xs,text->ys, text->xe,text->ye,255);
    }
    //---------文字---------
    gui->text= text;        //GUI局部刷新
    SC_Show_text(text->xs+offsx, text->ys+offsy,str, 0);
    gui->text= NULL;                        //注销
    gui->fc=fc;
    gui->alpha=255;
}



#if (SC_LVGL_FONT_EN&&SC_FXAA_EN)       //使用LVGL
#include "lvgl.h"
////LVGL字库工具生成，查找算法
static int binsearch(const uint16_t *sortedSeq, int seqLength, uint16_t keyData)
{
    int low = 0, mid, high = seqLength - 1;
    while (low <= high)
    {
        mid = (low + high)>>1;//右移1位等于是/2，奇数，无论奇偶，有个值就行
        if (keyData < sortedSeq[mid])
        {
            high = mid - 1;//是mid-1，因为mid已经比较过了
        }
        else if (keyData > sortedSeq[mid])
        {
            low = mid + 1;
        }
        else
        {
            return mid;
        }
    }
    return -1;
}
//LVGL字库工具生成，回调函数
const uint8_t * __user_font_get_bitmap(const lv_font_t * font, uint32_t unicode_letter)
{
    lv_font_fmt_txt_dsc_t * fdsc = (lv_font_fmt_txt_dsc_t *) font->dsc;

    if( unicode_letter<fdsc->cmaps[0].range_start || unicode_letter>fdsc->cmaps[0].range_length ) return false;

    int i;
    if( unicode_letter==fdsc->last_letter )
    {
        i = fdsc->last_glyph_id;
    }
    else
    {
        i = binsearch(fdsc->cmaps[0].unicode_list, fdsc->cmaps[0].list_length, unicode_letter);
    }
    if( i != -1 )
    {
        const lv_font_fmt_txt_glyph_dsc_t * gdsc = &fdsc->glyph_dsc[i];
        fdsc->last_glyph_id = i;
        fdsc->last_letter = unicode_letter;
        return &fdsc->glyph_bitmap[gdsc->bitmap_index];
    }
    return NULL;
}
//LVGL字库工具生成，回调函数
bool __user_font_get_glyph_dsc(const lv_font_t * font, lv_font_glyph_dsc_t * dsc_out, uint32_t unicode_letter, uint32_t unicode_letter_next)
{
    lv_font_fmt_txt_dsc_t * fdsc = (lv_font_fmt_txt_dsc_t *) font->dsc;
    if( unicode_letter<fdsc->cmaps[0].range_start || unicode_letter>fdsc->cmaps[0].range_length ) return false;
    int i;
    if( unicode_letter==fdsc->last_letter )
    {
        i = fdsc->last_glyph_id;
    }
    else
    {
        i = binsearch(fdsc->cmaps[0].unicode_list, fdsc->cmaps[0].list_length, unicode_letter);
    }
    if( i != -1 )
    {
        const lv_font_fmt_txt_glyph_dsc_t * gdsc = &fdsc->glyph_dsc[i];
        fdsc->last_glyph_id = i;
        fdsc->last_letter = unicode_letter;
        dsc_out->adv_w = gdsc->adv_w;
        dsc_out->box_h = gdsc->box_h;
        dsc_out->box_w = gdsc->box_w;
        dsc_out->ofs_x = gdsc->ofs_x;
        dsc_out->ofs_y = gdsc->ofs_y;
        dsc_out->bpp   = fdsc->bpp;
        return true;
    }
    return false;
}

//将utf-8编码转为unicode编码（函数来自LVGL）
static uint32_t lv_txt_utf8_next(const char * txt, uint32_t * i)
{
    uint32_t result = 0;
    uint32_t i_tmp = 0;
    if(i == NULL) i = &i_tmp;
    if((txt[*i] & 0x80) == 0)     //Normal ASCII
    {
        result = txt[*i];
        (*i)++;
    }
    else    //Real UTF-8 decode
    {
        // bytes UTF-8 code
        if((txt[*i] & 0xE0) == 0xC0)
        {
            result = (uint32_t)(txt[*i] & 0x1F) << 6;
            (*i)++;
            if((txt[*i] & 0xC0) != 0x80) return 0; //Invalid UTF-8 code
            result += (txt[*i] & 0x3F);
            (*i)++;
        }
        //3 bytes UTF-8 code
        else if((txt[*i] & 0xF0) == 0xE0)
        {
            result = (uint32_t)(txt[*i] & 0x0F) << 12;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += (uint32_t)(txt[*i] & 0x3F) << 6;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += (txt[*i] & 0x3F);
            (*i)++;
        }

        else if((txt[*i] & 0xF8) == 0xF0)
        {
            result = (uint32_t)(txt[*i] & 0x07) << 18;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += (uint32_t)(txt[*i] & 0x3F) << 12;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += (uint32_t)(txt[*i] & 0x3F) << 6;
            (*i)++;

            if((txt[*i] & 0xC0) != 0x80) return 0;
            result += txt[*i] & 0x3F;
            (*i)++;
        }
        else
        {
            (*i)++;
        }
    }
    return result;
}
//LVGL抗锯齿显示单字
void lv_draw_letter(u16 x, u16 y, lv_font_glyph_dsc_t *gdsc,uint32_t unicode,lv_font_t * font)
{
    u16 height=font->line_height;
    if(gdsc->box_w*gdsc->box_h==0)
    {
        if(gui->text)
        {
            SC_FillFrame(x,gui->text->ys,x+gdsc->box_w,gui->text->ye-1,0);
        }
        else
        {
            SC_FillFrame(x,y,x+gdsc->box_w,y+height,0); //空格退出
        }
        return;
    }
    //-------------取字库数据---------------
    u8 *map_p =__user_font_get_bitmap(font,unicode);
    y+=(height - font->base_line) - gdsc->box_h - gdsc->ofs_y;
    x+= gdsc->ofs_x;
    SC_Show_png(x,y, gdsc->box_w, gdsc->box_h, map_p,gdsc->bpp);
}

//LVGL抗锯齿显示单符串或LVGL图标
//code:0横向显示，1 纵向显示，其他 图标编码，
void SC_Show_text(s16 x,s16 y,const char* txt, u16 code)
{
    lv_font_t* font=(lv_font_t*)gui->font;
    uint32_t i =0;
    uint32_t unicode;
    lv_font_glyph_dsc_t g;       //lv_font_glyph_dsc_t g;  //描述符号的属性。
    do
    {
        if(code>1)
        {
            unicode=code;                     //用户指定unicode
        }
        else
        {
            unicode = lv_txt_utf8_next(txt,&i); //txt转unicode
        }
        if(__user_font_get_glyph_dsc(font,&g,unicode,0))
        {
            lv_draw_letter(x,y,&g,unicode,font);
            if(code==0)
            {
                x+=g.adv_w;
            }
            else if(code==1)
            {
                y+=font->line_height;
            }
        }
        else
        {
            return;
        }
    }
    while(unicode);
}
#else

//显示字符串中英混合
void SC_Show_text(int xs, int ys,char *str,u16 code)
{
    u16 k;
    char *p;
    SC_FONT  *font=(SC_FONT*)gui->font;
    while(*str)
    {
        if((*str<='~')&&(*str>=' '))        //判断是不是非法字符!
        {
            font=(SC_FONT*)gui->font;
            if(font->List)                   //搜索字表
            {
                for (k=0; font->List[k]; k++)
                {
                    if ((font->List[k]==*(str)))
                    {
                        SC_Show_png(xs,ys+font->offsy,font->w,font->h,&font->Tab_buf[k* font->Tab_len],1);
                        break;
                    }
                }
            }
            else
            {
                k=(*str)-' ';                //全字库
                SC_Show_png(xs,ys+font->offsy,font->w,font->h,&font->Tab_buf[k* font->Tab_len],1);
            }
            xs+=font->w;
            str++;
        }
        else
        {
            font=(SC_FONT*)gui->font+1;             //汉字
            if(font->List)                  //搜索字库中文注意编码
            {
                for (k=0; font->List[k]; k++) //utf8+=3 ,gbk+=2
                {
                    p=&font->List[k*3];
                    if (p[0]==str[0]&&p[1]==str[1]&&p[2]==str[2])
                    {
                        SC_Show_png(xs,ys+font->offsy,font->w,font->h,&font->Tab_buf[k* font->Tab_len],1);
                        break;
                    }
                }
            }
            xs+=font->w;
            str+=3;                                 //utf8
        }
    }

}

//***********************12_06全部字体*****************************
/*取模设置，阴码，顺向C51取模，二维数组*/
//数字，英文，中文自己增加
const unsigned char font_1206_En[95][12]=
{
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*" ",0*/
    {0x00,0x00,0x20,0x20,0x20,0x20,0x20,0x00,0x00,0x20,0x00,0x00},/*"!",1*/
    {0x28,0x28,0x50,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*""",2*/
    {0x00,0x00,0x50,0x50,0xF8,0x50,0x50,0xF8,0x50,0x50,0x00,0x00},/*"#",3*/
    {0x00,0x20,0x70,0xA8,0xA0,0x60,0x30,0x28,0xA8,0x70,0x20,0x00},/*"$",4*/
    {0x00,0x00,0x48,0xA8,0xB0,0xA8,0x74,0x34,0x54,0x48,0x00,0x00},/*"%",5*/
    {0x00,0x00,0x20,0x50,0x50,0x6C,0xA8,0xA8,0x94,0x68,0x00,0x00},/*"&",6*/
    {0x40,0x40,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*"'",7*/
    {0x08,0x10,0x10,0x20,0x20,0x20,0x20,0x20,0x10,0x10,0x08,0x00},/*"(",8*/
    {0x40,0x20,0x20,0x10,0x10,0x10,0x10,0x10,0x20,0x20,0x40,0x00},/*")",9*/
    {0x00,0x00,0x00,0x20,0xA8,0x70,0x70,0xA8,0x20,0x00,0x00,0x00},/*"*",10*/
    {0x00,0x00,0x00,0x10,0x10,0x7C,0x10,0x10,0x00,0x00,0x00,0x00},/*"+",11*/
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x40,0x40,0x80,0x00},/*",",12*/
    {0x00,0x00,0x00,0x00,0x00,0xFC,0x00,0x00,0x00,0x00,0x00,0x00},/*"-",13*/
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x40,0x00,0x00},/*".",14*/
    {0x00,0x04,0x08,0x08,0x10,0x10,0x20,0x20,0x40,0x40,0x80,0x00},/*"/",15*/
    {0x00,0x00,0x70,0x88,0x88,0x88,0x88,0x88,0x88,0x70,0x00,0x00},/*"0",16*/
    {0x00,0x00,0x20,0x60,0x20,0x20,0x20,0x20,0x20,0x70,0x00,0x00},/*"1",17*/
    {0x00,0x00,0x70,0x88,0x88,0x10,0x20,0x40,0x80,0xF8,0x00,0x00},/*"2",18*/
    {0x00,0x00,0x70,0x88,0x08,0x30,0x08,0x08,0x88,0x70,0x00,0x00},/*"3",19*/
    {0x00,0x00,0x10,0x30,0x30,0x50,0x90,0xF8,0x10,0x38,0x00,0x00},/*"4",20*/
    {0x00,0x00,0xF8,0x80,0x80,0xF0,0x88,0x08,0x88,0x70,0x00,0x00},/*"5",21*/
    {0x00,0x00,0x30,0x48,0x80,0xB0,0xC8,0x88,0x88,0x70,0x00,0x00},/*"6",22*/
    {0x00,0x00,0x78,0x08,0x10,0x10,0x20,0x20,0x20,0x20,0x00,0x00},/*"7",23*/
    {0x00,0x00,0x70,0x88,0x88,0x70,0x88,0x88,0x88,0x70,0x00,0x00},/*"8",24*/
    {0x00,0x00,0x70,0x88,0x88,0x98,0x68,0x08,0x90,0x60,0x00,0x00},/*"9",25*/
    {0x00,0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x00,0x20,0x00,0x00},/*":",26*/
    {0x00,0x00,0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x20,0x20,0x00},/*";",27*/
    {0x00,0x00,0x08,0x10,0x20,0x40,0x40,0x20,0x10,0x08,0x00,0x00},/*"<",28*/
    {0x00,0x00,0x00,0x00,0xFC,0x00,0xFC,0x00,0x00,0x00,0x00,0x00},/*"=",29*/
    {0x00,0x00,0x40,0x20,0x10,0x08,0x08,0x10,0x20,0x40,0x00,0x00},/*">",30*/
    {0x00,0x00,0x70,0x88,0x88,0x10,0x20,0x20,0x00,0x20,0x00,0x00},/*"?",31*/
    {0x00,0x00,0x38,0x44,0x94,0xB4,0xB4,0xB8,0x44,0x38,0x00,0x00},/*"@",32*/
    {0x00,0x00,0x20,0x20,0x30,0x50,0x50,0x78,0x48,0xCC,0x00,0x00},/*"A",33*/
    {0x00,0x00,0xF0,0x48,0x48,0x70,0x48,0x48,0x48,0xF0,0x00,0x00},/*"B",34*/
    {0x00,0x00,0x78,0x88,0x80,0x80,0x80,0x80,0x88,0x70,0x00,0x00},/*"C",35*/
    {0x00,0x00,0xF0,0x48,0x48,0x48,0x48,0x48,0x48,0xF0,0x00,0x00},/*"D",36*/
    {0x00,0x00,0xF8,0x48,0x50,0x70,0x50,0x40,0x48,0xF8,0x00,0x00},/*"E",37*/
    {0x00,0x00,0xF8,0x48,0x50,0x70,0x50,0x40,0x40,0xE0,0x00,0x00},/*"F",38*/
    {0x00,0x00,0x38,0x48,0x80,0x80,0x9C,0x88,0x48,0x30,0x00,0x00},/*"G",39*/
    {0x00,0x00,0xCC,0x48,0x48,0x78,0x48,0x48,0x48,0xCC,0x00,0x00},/*"H",40*/
    {0x00,0x00,0xF8,0x20,0x20,0x20,0x20,0x20,0x20,0xF8,0x00,0x00},/*"I",41*/
    {0x00,0x00,0x7C,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x90,0xE0},/*"J",42*/
    {0x00,0x00,0xEC,0x48,0x50,0x60,0x50,0x48,0x48,0xEC,0x00,0x00},/*"K",43*/
    {0x00,0x00,0xE0,0x40,0x40,0x40,0x40,0x40,0x44,0xFC,0x00,0x00},/*"L",44*/
    {0x00,0x00,0xDC,0xD8,0xD8,0xD8,0xA8,0xA8,0xA8,0xAC,0x00,0x00},/*"M",45*/
    {0x00,0x00,0xDC,0x48,0x68,0x68,0x58,0x58,0x48,0xE8,0x00,0x00},/*"N",46*/
    {0x00,0x00,0x70,0x88,0x88,0x88,0x88,0x88,0x88,0x70,0x00,0x00},/*"O",47*/
    {0x00,0x00,0xF0,0x48,0x48,0x70,0x40,0x40,0x40,0xE0,0x00,0x00},/*"P",48*/
    {0x00,0x00,0x70,0x88,0x88,0x88,0x88,0xE8,0x98,0x70,0x18,0x00},/*"Q",49*/
    {0x00,0x00,0xF0,0x48,0x48,0x70,0x50,0x48,0x48,0xEC,0x00,0x00},/*"R",50*/
    {0x00,0x00,0x78,0x88,0x80,0x60,0x10,0x08,0x88,0xF0,0x00,0x00},/*"S",51*/
    {0x00,0x00,0xF8,0xA8,0x20,0x20,0x20,0x20,0x20,0x70,0x00,0x00},/*"T",52*/
    {0x00,0x00,0xCC,0x48,0x48,0x48,0x48,0x48,0x48,0x30,0x00,0x00},/*"U",53*/
    {0x00,0x00,0xCC,0x48,0x48,0x50,0x50,0x30,0x20,0x20,0x00,0x00},/*"V",54*/
    {0x00,0x00,0xA8,0xA8,0xA8,0xA8,0x70,0x50,0x50,0x50,0x00,0x00},/*"W",55*/
    {0x00,0x00,0xD8,0x50,0x50,0x20,0x20,0x50,0x50,0xD8,0x00,0x00},/*"X",56*/
    {0x00,0x00,0xD8,0x50,0x50,0x50,0x20,0x20,0x20,0x70,0x00,0x00},/*"Y",57*/
    {0x00,0x00,0xF8,0x90,0x10,0x20,0x20,0x40,0x48,0xF8,0x00,0x00},/*"Z",58*/
    {0x38,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x38,0x00},/*"[",59*/
    {0x00,0x40,0x40,0x20,0x20,0x20,0x10,0x10,0x10,0x08,0x08,0x00},/*"\",60*/
    {0x70,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x70,0x00},/*"]",61*/
    {0x20,0x50,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*"^",62*/
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFC},/*"_",63*/
    {0x40,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*"`",64*/
    {0x00,0x00,0x00,0x00,0x00,0x30,0x48,0x38,0x48,0x3C,0x00,0x00},/*"a",65*/
    {0x00,0xC0,0x40,0x40,0x40,0x70,0x48,0x48,0x48,0x70,0x00,0x00},/*"b",66*/
    {0x00,0x00,0x00,0x00,0x00,0x38,0x48,0x40,0x48,0x30,0x00,0x00},/*"c",67*/
    {0x00,0x18,0x08,0x08,0x08,0x38,0x48,0x48,0x48,0x3C,0x00,0x00},/*"d",68*/
    {0x00,0x00,0x00,0x00,0x00,0x30,0x48,0x78,0x40,0x38,0x00,0x00},/*"e",69*/
    {0x00,0x18,0x24,0x20,0x20,0x78,0x20,0x20,0x20,0x78,0x00,0x00},/*"f",70*/
    {0x00,0x00,0x00,0x00,0x00,0x3C,0x48,0x30,0x40,0x38,0x44,0x38},/*"g",71*/
    {0x00,0xC0,0x40,0x40,0x40,0x70,0x48,0x48,0x48,0xEC,0x00,0x00},/*"h",72*/
    {0x00,0x20,0x20,0x00,0x00,0x60,0x20,0x20,0x20,0x70,0x00,0x00},/*"i",73*/
    {0x00,0x10,0x10,0x00,0x00,0x30,0x10,0x10,0x10,0x10,0x10,0xE0},/*"j",74*/
    {0x00,0xC0,0x40,0x40,0x40,0x58,0x50,0x60,0x50,0xC8,0x00,0x00},/*"k",75*/
    {0x00,0xE0,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0xF8,0x00,0x00},/*"l",76*/
    {0x00,0x00,0x00,0x00,0x00,0xF0,0xA8,0xA8,0xA8,0xA8,0x00,0x00},/*"m",77*/
    {0x00,0x00,0x00,0x00,0x00,0xF0,0x48,0x48,0x48,0xEC,0x00,0x00},/*"n",78*/
    {0x00,0x00,0x00,0x00,0x00,0x30,0x48,0x48,0x48,0x30,0x00,0x00},/*"o",79*/
    {0x00,0x00,0x00,0x00,0x00,0xF0,0x48,0x48,0x48,0x70,0x40,0xE0},/*"p",80*/
    {0x00,0x00,0x00,0x00,0x00,0x38,0x48,0x48,0x48,0x38,0x08,0x1C},/*"q",81*/
    {0x00,0x00,0x00,0x00,0x00,0xD8,0x60,0x40,0x40,0xE0,0x00,0x00},/*"r",82*/
    {0x00,0x00,0x00,0x00,0x00,0x78,0x40,0x30,0x08,0x78,0x00,0x00},/*"s",83*/
    {0x00,0x00,0x00,0x20,0x20,0x78,0x20,0x20,0x20,0x38,0x00,0x00},/*"t",84*/
    {0x00,0x00,0x00,0x00,0x00,0xD8,0x48,0x48,0x48,0x3C,0x00,0x00},/*"u",85*/
    {0x00,0x00,0x00,0x00,0x00,0xD8,0x50,0x50,0x20,0x20,0x00,0x00},/*"v",86*/
    {0x00,0x00,0x00,0x00,0x00,0xA8,0xA8,0x70,0x50,0x50,0x00,0x00},/*"w",87*/
    {0x00,0x00,0x00,0x00,0x00,0xD8,0x50,0x20,0x50,0xD8,0x00,0x00},/*"x",88*/
    {0x00,0x00,0x00,0x00,0x00,0xCC,0x48,0x48,0x30,0x10,0x20,0xC0},/*"y",89*/
    {0x00,0x00,0x00,0x00,0x00,0x78,0x10,0x20,0x20,0x78,0x00,0x00},/*"z",90*/
    {0x18,0x10,0x10,0x10,0x10,0x30,0x10,0x10,0x10,0x10,0x18,0x00},/*"{",91*/
    {0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10},/*"|",92*/
    {0x60,0x20,0x20,0x20,0x20,0x10,0x20,0x20,0x20,0x20,0x60,0x00},/*"}",93*/
    {0x68,0x90,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},/*"~",94*/
};

const unsigned char _font20_Ch[][60]=
{

    0x00,0x00,0x00,0x20,0x00,0x40,0x1B,0xF0,0x40,0x0A,0x10,0x40,0x02,0x12,0x40,0x02,
    0x52,0x40,0x26,0x52,0x40,0x26,0x52,0x40,0x06,0x52,0x40,0x0A,0x52,0x40,0x0A,0x52,
    0x40,0x0A,0x52,0x40,0x12,0x92,0x40,0x72,0x92,0x40,0x10,0x80,0x40,0x11,0x20,0x40,
    0x31,0x10,0x40,0x36,0x19,0xC0,0x08,0x00,0x80,0x00,0x00,0x00,/*"测",2*/

    0x00,0x00,0x00,0x00,0x05,0x00,0x00,0x06,0x80,0x18,0x04,0xC0,0x08,0x04,0x40,0x09,
    0xFF,0xE0,0x00,0x04,0x00,0x00,0x04,0x00,0x38,0x04,0x00,0x08,0xDE,0x00,0x08,0x22,
    0x00,0x08,0x22,0x00,0x08,0x22,0x00,0x09,0x23,0x00,0x0A,0x25,0x20,0x0A,0x39,0xA0,
    0x0D,0xC0,0xA0,0x08,0x00,0x60,0x00,0x00,0x20,0x00,0x00,0x00,/*"试",3*/

};

//外部声明字库
const SC_FONT font_1206[2]=
{
    {
        //英文
        12,
        6,
        8,                          //offsy偏移
        (u8*)font_1206_En,			//取模
        sizeof(font_1206_En[0]),  	//长度
        NULL,                       //全字写NULL
    },
    {
        //中文
        20,
        20,
        0,                          //offsy
        (u8*)_font20_Ch,			//取模
        sizeof(_font20_Ch[0]),  	//长度
        "测试",                     //字表
    },
};

#endif

