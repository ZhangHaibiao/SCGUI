#ifndef _SC_CHART_H
#define _SC_CHART_H

#include "scgui.h"



void SC_FXAA_DrawLine( int x1, int y1, int x2, int y2,u8 w_Draw);

void SC_OBJ_chart_cfg(SC_OBJ* chart,int xs,int ys,u16 w,u16 h,u16 pix_num);

void SC_OBJ_chart_put_dat(SC_OBJ *chart,s16  *buf,s16 dat);

void SC_OBJ_chart_Show(SC_OBJ *chart,s16 *buf);


#endif
