
#include "sc_chart.h"
#include "scgui.h"

extern void put_pset(int a);

//反锯齿画线,笔粗可以调,
void SC_FXAA_DrawLine( int x1, int y1, int x2, int y2,u8 w_Draw)
{
    int  dxabs, dyabs,sgndx=1, sgndy=1;
    int  i=1,x=0, y=0,a=0;
    dxabs = x2 - x1;         //宽度
    dyabs = y2 - y1;         //高度
    if(dxabs<0)
    {
        dxabs=-dxabs;
        sgndx=-1;
    }
    if(dyabs<0)
    {
        dyabs=-dyabs;
        sgndy=-1;
    }
    if( dxabs >= dyabs )                 //横线斜率
    {
        while(1)
        {
            if(sgndy>0)  a=255-a;
            if(dxabs == dyabs )
            {
                put_pset(150);
                a=200;
            }
            put_pset(a);
            for( i; i<w_Draw; i++)
            {
                put_pset(255);
            }
            put_pset(255-a);
            LcdRefresh(x1, y1,x1, y1+i);   //x不变
            if(x1==x2) break;
            y += dyabs;
            if( y >= dxabs )
            {
                y -= dxabs;
                y1 += sgndy;
            }
            x1+=sgndx;
            a=((y%dxabs)<<8)/dxabs;
        }
    }
    else                                    //竖线斜率
    {
        while(1)
        {
            if(sgndx>0) a=255-a;
            put_pset(a);
            for(i; i<w_Draw; i++)
            {
                put_pset(255);
            }
            put_pset(255-a);
            LcdRefresh(x1, y1,x1+i, y1);   //y不变
            if(y1==y2) break;
            x += dxabs;
            if( x >= dyabs )
            {
                x -= dyabs;
                x1 += sgndx;
            }
            y1+= sgndy;
            a=((x%dyabs)<<8)/dyabs;
        }
    }
}


//波形线性插值输出放大256倍
void _bilinear_bmp(s16 *src,s16 *dst,u32 src_width, u32 dst_width)
{
    u32  j,x;
    s32  result;
    s32  x_diff, x_diff_comp;           //权重
    for ( j = 0; j < dst_width; j++)
    {
        u32  x_ratio = (j<<8)*(src_width-1) / (dst_width-1); //缩放系数,防止边界问题指针超出
        x =  x_ratio >> 8;                                   //y坐标取整
        x_diff = x_ratio - (x << 8);                         //坐标取余
        x_diff_comp = (1 << 8) - x_diff;                     //1-余
        //-----------邻值取权重--------------
        result = x_diff_comp*src[x];        //x,y;
        result+= x_diff *src[x + 1];        //x+1,y;
        dst[j]=result>>8;
    }
}

void SC_OBJ_chart_cfg(SC_OBJ* chart,int xs,int ys,u16 w,u16 h,u16 pix_num)
{
    chart->xs=xs;
    chart->ys=ys;
    chart->xe=xs+w-1;
    chart->ye=ys+h-1;
    if(pix_num>120) pix_num=120;
    chart->r=pix_num;
}
//压入数据
void SC_OBJ_chart_put_dat(SC_OBJ *chart,s16 *buf,s16 dat)
{
    u16 x;
    for(x=1; x<chart->r; x++)
    {
        buf[x-1]=buf[x];          //波形前移
    }
    buf[x-1]=dat;                 //入队列
}
//显示波形
void SC_OBJ_chart_Show(SC_OBJ *chart, s16 *buf)
{
    u16 x,y;
    u16 w=chart->xe-chart->xs+1;          //窗口宽度
    u16 h=chart->ye-chart->ys+1;          //窗口宽度
    s16 out_buf[240];                     //输出数据窗口宽度
    s16 min,max;
    _bilinear_bmp(buf,out_buf,chart->r,w);//对波形进行插值放大
    for(x=0; x<w-1; x++)
    {
        if(out_buf[x]>out_buf[x+1])
        {
            max=out_buf[x];
            min=out_buf[x+1];
        }
        else
        {
            min=out_buf[x];
            max=out_buf[x+1];
        }
        for(y=0; y<h; y++)
        {
            if(y<min||y>max)
            {
                put_pset(0);
            }
            else
            {
                put_pset(255);
            }
        }
        LcdRefresh(chart->xs+x, chart->ys,chart->xs+x, chart->ye);
    }
}

#if 0




/******位图画布,320*240/8 约9K内存************************/
static u8  bit_buf[LCD_SCREEN_HEIGHT][LCD_SCREEN_WIDTH/8];

//位图画点，0，1
void LCD_bit_pset(s16 xs,s16 ys,u16 dat)
{
    u16 temp,xb,x;
    xs=(xs+240)%240;
    ys=(ys+320)%320;
    xb=xs%8;
    x=xs/8;
    temp=bit_buf[ys][x];
    if(dat)
    {
        temp|=(0x80>>xb);
    }
    else
    {
        temp&=~(0x80>>xb);
    }
    bit_buf[ys][x]=temp;
}
//位图区域填充
void LCD_bit_fill(s16 xs,s16 ys,u16 w,u16 h,u8 dat)
{
    u16 temp=0;
    s16 xe=xs+w-1;
    s16 ye=ys+h-1;
    u16 xa=xs%8;
    u16 xb=xe%8;
    xs=xs/8;
    xe=xe/8;
    if(dat) dat=0xff;
    for(ys; ys<=ye; ys++)
    {
        temp=xs;
        if(xa)      //xs不对齐处理
        {
            temp=bit_buf[ys][xs];
            if(dat)
            {
                temp|=(0xff>>xa);
            }
            else
            {
                temp&=~(0xff>>xa);
            }
            bit_buf[ys][xs]=temp;
            temp=xs+1;
        }
        for(temp; temp<xe; temp++)   //对齐copy
        {
            bit_buf[ys][temp]=dat;
        }
        if(xb)      //xe不对齐处理
        {
            temp=bit_buf[ys][xe];
            if(dat)
            {
                temp|=(0xff00>>xb);
            }
            else
            {
                temp&=~(0xff00>>xb);
            }
            bit_buf[ys][xe]=temp;
        }
    }
}
void lcd_buf_copy(u16 xs,s16 ys,u16 w,u16 h,u8* buf,u8 mode)
{
    u8 *map,dat[2];
    s16 xe=xs+w;
    s16 ye=ys+h;
    u16 xa=(xs%8);
    u16 xb=(xe)%8;
    xs=xs/8;
    xe=xe/8;
    w=(w+7)/8;
    for(s16 x=xs; x<=xe; x++)       //加一个循环
    {
        map=buf;
        for(s16 y=ys; y<ye; y++)
        {
            if(x==xs)
            {
                dat[1]=bit_buf[y][x]>>(8-xa);
            }
            else
            {
                dat[1]=*(map-1);
            }
            dat[0]=*map;
            dat[0]=*(u16*)dat>>xa;

            map+=w;          //下一行
            if(x==xe)       //处理最后不对齐
            {
                if(xb)
                {
                    bit_buf[y][x]&=(0xff>>xb);
                    bit_buf[y][x]|=dat[0];
                    continue;
                }
                else
                {
                    break;      //退出
                }
            }
            bit_buf[y][x]=dat[0];
        }
        buf++;
    }

}
//全屏刷新
void LCD_bit_Refresh(void)
{
    if(gui->gui_pset==LCD_bit_pset)
    {
        gui->gui_pset=dma_pset;
        SC_Show_png(0,0,240,320, bit_buf,1);
        gui->gui_pset=LCD_bit_pset;
    }
}

#endif
