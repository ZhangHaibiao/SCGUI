#include  <SDL2/SDL.h>
#include  <SDL2/SDL_keyboard.h>
#include  <SDL2/SDL_keycode.h>
#include  <stdio.h>

SDL_Window* gWindow = NULL;
SDL_Surface* gScreenSurface = NULL;
SDL_Renderer* gRenderer = NULL;


#include "scgui.h"

const int SCREEN_WIDTH = LCD_SCREEN_WIDTH;
const int SCREEN_HEIGHT = LCD_SCREEN_HEIGHT;



int init_SDL2(void)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL can not initialized!SDL Error:%s\n", SDL_GetError());
        return 0;
    }
    //放大像素
    gWindow = SDL_CreateWindow("UC_GUI", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH*2, SCREEN_HEIGHT*2, SDL_WINDOW_SHOWN);
    if (gWindow == NULL)
    {
        printf("SDL can't create window!SDL Error:%s\n", SDL_GetError());
        return -1;
    }

    gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
    if (gRenderer == NULL)
    {
        return 0;
    }
    //----------------SDL2---------------------------------//
    SDL_RenderClear(gRenderer);
    SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xff);     // bg
    printf("open WindowShow \n");
    return 1;
}

void close_SDL2(void)
{

    SDL_DestroyRenderer(gRenderer);
    SDL_DestroyWindow(gWindow);
    SDL_Quit();
}

//GB565 画一个点
void SDL_DrawPoint ( int x, int y, int c )
{
    Uint8 r,g,b;
    /* Convert RGB565 to RGB888 */
    r = (c>>11)&0x1F;
    r<<=3;
    g = (c>>5)&0x3F;
    g<<=2;
    b = (c)&0x1F;
    b<<=3;
    SDL_SetRenderDrawColor(gRenderer,(Uint8)r, (Uint8)g, (Uint8)b, 0xff);
#if 1
    SDL_RenderDrawPoint(gRenderer, 2*x,2*y);        //放大像素
    SDL_RenderDrawPoint(gRenderer, 2*x+1,2*y);
    SDL_RenderDrawPoint(gRenderer, 2*x,2*y+1);
    SDL_RenderDrawPoint(gRenderer, 2*x+1,2*y+1);
#else
    SDL_RenderDrawPoint(gRenderer, x,y);             //1:1像素
#endif // 1

}
//
//---------------------------事件---------------------------------------------
int get_SDL_Event(void)       //键盘事件
{
    SDL_Event evt;
    while (SDL_PollEvent(&evt) != 0)
    {
        //event.type存储了当前的事件类型
        //如果无键盘鼠标 触摸点击 那么 默认是 0x200
        switch (evt.type)
        {
        case SDL_KEYDOWN:   //按下

            printf("\nk=%s",  SDL_GetKeyName(evt.key.keysym.sym));
            const Uint8 *state = SDL_GetKeyboardState(NULL);
            char k=0;
            if ( state[SDL_SCANCODE_UP])   k='U';
            if ( state[SDL_SCANCODE_DOWN] )k='D';
            if ( state[SDL_SCANCODE_LEFT] )k='L';
            if ( state[SDL_SCANCODE_RIGHT])k='R';

            if ( state[SDL_SCANCODE_0] )k='0';
            if ( state[SDL_SCANCODE_1] )k='1';
            if ( state[SDL_SCANCODE_2] )k='2';
            if ( state[SDL_SCANCODE_3] )k='3';
            if ( state[SDL_SCANCODE_4] )k='4';
            if ( state[SDL_SCANCODE_5] )k='5';
            if ( state[SDL_SCANCODE_6] )k='6';
            if ( state[SDL_SCANCODE_7] )k='7';
            if ( state[SDL_SCANCODE_8] )k='8';
            if ( state[SDL_SCANCODE_9] )k='9';

            if ( state[SDL_SCANCODE_A]) k='~';
            if ( state[SDL_SCANCODE_S]) k='+';
            if ( state[SDL_SCANCODE_D]) k='-';
            if ( state[SDL_SCANCODE_F]) k='*';
            if ( state[SDL_SCANCODE_G]) k='/';

            if ( state[SDL_SCANCODE_PAGEDOWN])  k='O';  //回车
            if ( state[SDL_SCANCODE_PAGEUP])    k='B';  //清除
            if ( state[SDL_SCANCODE_DELETE])    k='d';  //删除

        case SDL_KEYUP: //释放

            break;
        case SDL_TEXTINPUT:
            //如果是文本输入 输出文本
            // PrintText(event.text.text);
            break;
        //case SDL_MOUSEBUTTONDOWN:
        /* 如果有任何鼠标点击消息 或者SDL_QUIT消息 那么将退出窗口 */
        case SDL_QUIT:
            return -1;
            break;
        default:
            break;
        }
    }
    return 0;
}

SC_GUI  pc_gui;
SC_OBJ  text;
SC_OBJ  chart;
s16 in_buf[128];

int mainLoop(void)
{

    u8 t=10;
    SC_GUI_Init(&pc_gui,SDL_DrawPoint,C_YELLOW,C_BLACK,&lv_Font20);        //LVGL字库
//  SC_GUI_Init(&pc_gui, SDL_DrawPoint, C_RED, C_BLACK,&font_1206);        //取模字库

    SC_Show_Image(0,0, &test_jpg);      //图片
    SC_Show_text(90, 8,"SCGUI", 0);     //文字


    //----------按键---------------
    SC_OBJ_text_cfg(&text,160,280,60,30,8);
    SC_OBJ_text_show(&text,14,2,"NO", C_RED,C_WHITE,180);

    SC_OBJ_text_cfg(&text,10,280,60,30,8);
    SC_OBJ_text_show(&text,10,2,"YES",C_RED,C_BLUE,100);


    //----------波形---------------
    SC_OBJ_chart_cfg(&chart,20, 60,200, 44, sizeof(in_buf)/sizeof(in_buf[0]));

    //----------图形----------
    SC_FXAA_DrawCircle(50,220,40,1);        //实心
    SC_FXAA_RoundFrame(150,200,80,40,6,0);  //空心
    while (1)
    {

        SC_Show_text(t, 140,"使用LVGL", 0);     //文字
        SC_OBJ_chart_put_dat(&chart,in_buf,t%20);
        SC_OBJ_chart_Show(&chart,in_buf);
        if(++t>240)
        {
            t=0;
        }
        SDL_Delay(10);   /*Sleep for 5 millisecond*/
        SDL_RenderPresent(gRenderer);// 刷新窗口
        if(get_SDL_Event()==-1)
        {
            return -1;
        }
    }
    return 0;
}

/**
 * A task to measure the elapsed time for LittlevGL
 * @param data unused
 * @return never return
 */
static int tick_thread(void * data)
{
    (void)data;
    while(1)
    {
        SDL_Delay(5);   /*Sleep for 5 millisecond*/
    }
    return 0;
}
int main(int argc,char* args[])
{
    if (!init_SDL2()) return -1;
    SDL_CreateThread(tick_thread, "tick", NULL);    //定时线程
    mainLoop();
    close_SDL2();
    return 0;
}



